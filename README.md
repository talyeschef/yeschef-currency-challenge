# yeschef-currency-challenge

Welcome to YesChef currency challenge.
In this challenge you're expected to write a small web service for currency conversion.

The repository has a test file that is going to run on your service.\
Please take into consideraion that more tests will be ran on your code so cover every case in your solution.\
A good solution will take up to 2 hour.

Good luck!
YesChef team :) 